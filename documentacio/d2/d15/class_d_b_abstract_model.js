var class_d_b_abstract_model =
[
    [ "delete", "d2/d15/class_d_b_abstract_model.html#a13bdffdd926f26b825ea57066334ff01", null ],
    [ "edit", "d2/d15/class_d_b_abstract_model.html#a5cb75cbb16467eb1768837d126dc535b", null ],
    [ "execute_single_query", "d2/d15/class_d_b_abstract_model.html#ab80cf8116668763e3cf54b0e7cab9cae", null ],
    [ "get", "d2/d15/class_d_b_abstract_model.html#ac33ee765f5ad9f134540bac393721cfe", null ],
    [ "get_results_from_query", "d2/d15/class_d_b_abstract_model.html#a23a8d3178609a77b8f225654b452bd1f", null ],
    [ "set", "d2/d15/class_d_b_abstract_model.html#a89f017a6c12e98acec0d6833ea9c8994", null ],
    [ "$db_name", "d2/d15/class_d_b_abstract_model.html#a26dcb19f4431598ddd5f58147f131bee", null ],
    [ "$error_conn", "d2/d15/class_d_b_abstract_model.html#ac87722dc9f1712024645418369dc532c", null ],
    [ "$error_query", "d2/d15/class_d_b_abstract_model.html#aadaff7297df101dd7afd59da28e55209", null ],
    [ "$mensaje", "d2/d15/class_d_b_abstract_model.html#a0eb3db09c2634a632fc6f0e97f40b7f5", null ],
    [ "$query", "d2/d15/class_d_b_abstract_model.html#af59a5f7cd609e592c41dc3643efd3c98", null ],
    [ "$rows", "d2/d15/class_d_b_abstract_model.html#ace2ec39e7df3899fa8df9640ec274b03", null ]
];