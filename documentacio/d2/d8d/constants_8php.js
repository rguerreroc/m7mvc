var constants_8php =
[
    [ "DELETE_USER", "d2/d8d/constants_8php.html#a1bc634353e5d584e4f34f9adc4974906", null ],
    [ "EDIT_USER", "d2/d8d/constants_8php.html#a4a5a1906565070977cdff43ae9da3774", null ],
    [ "GET_USER", "d2/d8d/constants_8php.html#abccb79f88888461df55d904264b881d1", null ],
    [ "MODULO", "d2/d8d/constants_8php.html#a151e5d004071422acd5bafde663ca437", null ],
    [ "SET_USER", "d2/d8d/constants_8php.html#a366a831d6fe31efece60bf24d1f3d7ff", null ],
    [ "VIEW_DELETE_USER", "d2/d8d/constants_8php.html#afc17de2f67ec9e2e91649fbc9d080c6c", null ],
    [ "VIEW_EDIT_USER", "d2/d8d/constants_8php.html#a9c2a181eabce5b0609d19d2c6614aee9", null ],
    [ "VIEW_GET_USER", "d2/d8d/constants_8php.html#a803c8d6a87e6312f3d47e823e75959b3", null ],
    [ "VIEW_SET_USER", "d2/d8d/constants_8php.html#add9a2a4c17d1f3002b883dd3e6a94cdb", null ]
];