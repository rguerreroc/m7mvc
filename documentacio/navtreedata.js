var NAVTREE =
[
  [ "M7_MVC", "index.html", [
    [ "Estructuras de Datos", null, [
      [ "Estructura de datos", "annotated.html", "annotated" ],
      [ "Índice de estructura de datos", "classes.html", null ],
      [ "Jerarquía de la clase", "hierarchy.html", "hierarchy" ],
      [ "Campos de datos", "functions.html", [
        [ "Todo", "functions.html", null ],
        [ "Funciones", "functions_func.html", null ],
        [ "Variables", "functions_vars.html", null ]
      ] ]
    ] ],
    [ "Archivos", null, [
      [ "Lista de archivos", "files.html", "files" ],
      [ "Globales", "globals.html", [
        [ "Todo", "globals.html", null ],
        [ "Variables", "globals_vars.html", null ]
      ] ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"annotated.html"
];

var SYNCONMSG = 'click en deshabilitar sincronización';
var SYNCOFFMSG = 'click en habilitar sincronización';