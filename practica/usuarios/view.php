<?php
/**
* @file view.php
* @brief Contiene la clase UsuarioVista y sus metodos/funciones
* @author Eugenia Bahit
* @author Raul Guerrero
* @version 1.0
* @copyright CC 4.0 BY-NC-SA
* @date 2015-03-22
 */

/**
* @brief Clase UsuarioVista
 */
class UsuarioVista{
    private $html_vista;
    private $url_aplicacion = '/M07/uf2/practica/';
    private $diccionario;

    /**
        * @brief Metodo magico que hace de constructor para poder instanciar la clase
     */
    function __construct(){
        $this->diccionario = array(
            'subtitle'=>array(
                VIEW_SET_USER=>'Crear un nuevo usuario',
                VIEW_GET_USER=>'Buscar usuario',
                VIEW_DELETE_USER=>'Eliminar un usuario',
                VIEW_EDIT_USER=>'Modificar usuario'
            ),
            'links_menu'=>array(
                'VIEW_SET_USER'=>MODULO.VIEW_SET_USER.'/',
                'VIEW_GET_USER'=>MODULO.VIEW_GET_USER.'/',
                'VIEW_EDIT_USER'=>MODULO.VIEW_EDIT_USER.'/',
                'VIEW_DELETE_USER'=>MODULO.VIEW_DELETE_USER.'/'
            ),
            'form_actions'=>array(
                'SET'=>$this->url_aplicacion.MODULO.SET_USER.'/',
                'GET'=>$this->url_aplicacion.MODULO.GET_USER.'/',
                'EDIT'=>$this->url_aplicacion.MODULO.EDIT_USER.'/',
                'DELETE'=>$this->url_aplicacion.MODULO.DELETE_USER.'/'
            )
        );
    }

    /**
        * @brief Funcion que comprueba que plantilla debe retornar
        *
        * @param $form
        *
        * @return Devuelve la plantilla solicitada
     */
    private function get_template($form='get') {
        $file = '../site_media/html/user_'.$form.'.html';
        $template = file_get_contents($file);
        return $template;
    }

    /**
        * @brief Funcion que sustituye en la plantilla indicada los valores que se vana mostrar
        *
        * @param $html
        * @param $data
        *
        * @return Devuelve la plantilla con las sustituciones
     */
    private function render_dinamic_data($html, $data) {
        foreach ($data as $clave=>$valor) {
            $html = str_replace('{'.$clave.'}', $valor, $html);
        }
        return $html;
    }

    /**
        * @brief Metodo que rellena la propiedad 'html_vista' con los datos que debe mostrar la vista dependiendo de los valores con la que la solicitas
        *
        * @param $vista
        * @param $data
     */
    function retornar_vista($vista, $data=array()) {
        //global $diccionario;
        $this->html_vista = $this->get_template('template');
        $this->html_vista = str_replace('{subtitulo}', $this->diccionario['subtitle'][$vista], $this->html_vista);
        $this->html_vista = str_replace('{formulario}', $this->get_template($vista), $this->html_vista);
        $this->html_vista = $this->render_dinamic_data($this->html_vista, $this->diccionario['form_actions']);
        $this->html_vista = $this->render_dinamic_data($this->html_vista, $this->diccionario['links_menu']);
        $this->html_vista = $this->render_dinamic_data($this->html_vista, $data);

        // render {mensaje}
        if(array_key_exists('nombre', $data)&&
            array_key_exists('apellido', $data)&&
            $vista==VIEW_EDIT_USER) {
            $mensaje = 'Editar usuario '.$data['nombre'].' '.$data['apellido'];
        } else {
            if(array_key_exists('mensaje', $data)) {
                $mensaje = $data['mensaje'];
            } else {
                $mensaje = 'Datos del usuario:';
            }
        }
        $this->html_vista = str_replace('{mensaje}', $mensaje, $this->html_vista);
    }

    /**
        * @brief Metodo que muestra por pantalla los datos almacenados en la propiedad 'html_vista'
     */
    function mostrar_vista(){ print $this->html_vista; }

    /**
        * @brief Metodo que almacena en la propiedad 'url_aplicacion' la ruta indicada por parametro
        *
        * @param $url_aplicacion
     */
    function set_url_aplicacion($url_aplicacion){ $this->url_aplicacion = $url_aplicacion; }

    /**
        * @brief Metodo magico que libera la memoria tras es uso de la clase
     */
    function __destruct(){ unset($this); }
}
?>
