<?php
/**
* @file constants.php
* @brief Archivo que contiene las contantes que se usaran en el resto de archivos (clases)
* @author Eugenia Bahit
* @author Raul Guerrero
* @version 1.0
* @copyright CC 4.0 BY-NC-SA
* @date 2015-03-22
 */
const MODULO = 'usuarios/';

# controladores
const SET_USER = 'set';
const GET_USER = 'get';
const DELETE_USER = 'delete';
const EDIT_USER = 'edit';

# vistas
const VIEW_SET_USER = 'agregar';
const VIEW_GET_USER = 'buscar';
const VIEW_DELETE_USER = 'borrar';
const VIEW_EDIT_USER = 'modificar';
?>
