<?php
/**
* @file model.php
* @brief Contiene la subclase Usuario que hereda de DBAbstractModel y amplia sus metodos/funciones
* @author Eugenia Bahit
* @author Raul Guerrero
* @version 1.0
* @copyright CC 4.0 BY-NC-SA
* @date 2015-03-22
 */
require_once('../core/db_abstract_model.php');


/**
    * @brief Subclase Usuario que extiende y hereda de DBAbstractModel
 */
class Usuario extends DBAbstractModel {

    /**
        * @brief Propiedades de la clase
     */
    public $nombre;
    public $apellido;
    public $email;
    protected $clave;
    protected $id;


    # Traer datos de un usuario
    /**
        * @brief Metodo que dado un email comprueba en la base de datos si existe y guarda en las propiedades el resultado o muestra mensaje correspondiente
        *
        * @param $user_email
        */
    function get($user_email='') {
        if($user_email != '') {
            $this->query = "
                SELECT id, nombre, apellido, email, clave
                FROM usuarios
                WHERE email = '$user_email'
                ";
            $this->get_results_from_query();
        }

        if(count($this->rows) == 1) {
            foreach ($this->rows[0] as $propiedad=>$valor) {
                $this->$propiedad = $valor;
            }
            $this->mensaje = 'Usuario encontrado';
        } else {
            $this->mensaje = 'Usuario NO existente';
        }
    }

    /**
        * @brief Metodo que trata un array con los datos introducidos y realiza la insercion de estos en la base de datos
        *
        * @param $user_data
     */
    function set($user_data=array()) {
        if(array_key_exists('email', $user_data)) {
            $this->get($user_data['email']);
            if($user_data['email'] != $this->email) {
                foreach ($user_data as $campo=>$valor) {
                    $$campo = $valor;
                }
                $this->query = "
                    INSERT INTO     usuarios (nombre, apellido, email, clave)
                    VALUES          ('$nombre', '$apellido', '$email', '$clave')
                    ";
                $this->execute_single_query();
                $this->mensaje = 'Usuario agregado exitosamente';
            } else {
                $this->mensaje = 'El usuario ya existe';
            }
        } else {
            $this->mensaje = 'No se ha agregado al usuario';
        }
    }

    /**
        * @brief Metodo que permite la modificacion de los datos guardados en la BDD
        *
        * @param $user_data
        */
    function edit($user_data=array()) {
        foreach ($user_data as $campo=>$valor) {
            $$campo = $valor;
        }
        $this->query = "
            UPDATE      usuarios
            SET         nombre='$nombre',
                        apellido='$apellido'
            WHERE       email = '$email'
        ";
        $this->execute_single_query();
        if(!$this->error_query){ $this->mensaje= 'Usuario modificado';  /*modificat*/
        }else{
            $this->mensaje = 'Usuario NO modificado';
        }
    }

    /**
        * @brief Metodo que dado un email, eliminina al usuario de la BDD
        *
        * @param $user_email
        */
    function delete($user_email='') {
        // me he dejado el if
        $this->query = "
            SELECT   id
            FROM     usuarios
            WHERE    email = '$user_email'
            ";

        $this->get_results_from_query();
        if(count($this->rows) > 0) {
            $this->query = "
                     DELETE FROM     usuarios 
                     WHERE           email = '$user_email' ";
            $this->execute_single_query();
            if(!$this->error_query){ 
                $this->mensaje= "Usuario eliminado"; 
            }
        } else {
            $this->mensaje= "Usuario no existente";
        }

    }

    /**
        * @brief Metodo magico que hace de constructor para poder instanciar la clase
     */
    function __construct() {
        $this->db_name = 'usuarioseb';
    }

    /**
        * @brief Metodo magico que libera la memoria tras es uso de la clase
     */
    function __destruct() {
        unset($this);
    }
}
?>
