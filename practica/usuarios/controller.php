<?php
/**
* @file controller.php
* @brief Archivo que contiene la clase UsuarioController y los metodos que puede realizar
* @author Eugenia Bahit
* @author Raul Guerrero
* @version 1.0
* @copyright CC 4.0 BY-NC-SA
* @date 2015-03-09
 */
require_once('constants.php');
require_once('model.php');
require_once('view.php');

/**
    * @brief Clase UsuarioController
 */
class UsuarioController{
    /**
        * @brief Propiedades privadas ejercicio 15 - a/b
     */
    private $event;
    private $peticiones;

    /**
        * @brief Metodo handler
     */
    function handler() {
        $this->get_event();
        $user_data = $this->helper_user_data();
        $usuario = new Usuario();
        $vistausu = new UsuarioVista();

        switch ($this->event) {
        case SET_USER:
            $usuario->set($user_data);
            $data = array('mensaje'=>$usuario->mensaje);
            $vistausu->retornar_vista(VIEW_SET_USER, $data);
            break;
        case GET_USER:
            $usuario->get($user_data);
            if($usuario->email != null) { // Mal controlado
                $data = array(
                    'nombre'=>$usuario->nombre,
                    'apellido'=>$usuario->apellido,
                    'email'=>$usuario->email
                );
                $vistausu->retornar_vista(VIEW_EDIT_USER, $data);
            } else {
                $data = array('mensaje'=>$usuario->mensaje);
                $vistausu->retornar_vista(VIEW_GET_USER, $data);
            }
            break;
        case DELETE_USER:
            $usuario->delete($user_data['email']);
            $data = array('mensaje'=>$usuario->mensaje);
            $vistausu->retornar_vista(VIEW_DELETE_USER, $data);
            break;
        case EDIT_USER:
            $usuario->edit($user_data);
            $data = array('mensaje'=>$usuario->mensaje);
            $vistausu->retornar_vista(VIEW_GET_USER, $data);
            break;
        default:
            $vistausu->retornar_vista($this->event);
        }
        $vistausu->mostrar_vista();
    }

    /**
     * @brief Funcion que rellena una array con los datos introducidos por $_POST
     *
     * @return Devuelve un array con los datos del usuario
     *
     */
    function helper_user_data() {
        $user_data = array();
        if($_POST) {
            if(array_key_exists('nombre', $_POST)) { 
                $user_data['nombre'] = $_POST['nombre']; 
            }
            if(array_key_exists('apellido', $_POST)) { 
                $user_data['apellido'] = $_POST['apellido']; 
            }
            if(array_key_exists('email', $_POST)) { 
                $user_data['email'] = $_POST['email']; 
            }
            if(array_key_exists('clave', $_POST)) { 
                $user_data['clave'] = $_POST['clave']; 
            }
        } else if($_GET) {
            if(array_key_exists('email', $_GET)) {
                $user_data = $_GET['email'];
            }
        }
        return $user_data;
    }
    /**
     * @brief Metodo que comprueba el evento que se esta solicitando y si es valido lo alamacena en la propiedad correspodiente
     *
     * @return 
     */
    function get_event(){
        $this->event = VIEW_GET_USER;
        $uri = $_SERVER['REQUEST_URI'];
        foreach ($this->peticiones as $peticion) {
            $uri_peticion = MODULO.$peticion.'/';
            if( strpos($uri, $uri_peticion) == true ) {
                $this->event = $peticion;
            }
        }
    }

    /**
        * @brief Metodo magico que hace de constructor para poder instanciar la clase
     */
    function __construct(){
        $this->peticiones = array(SET_USER, GET_USER, DELETE_USER, EDIT_USER,
            VIEW_SET_USER, VIEW_GET_USER, VIEW_DELETE_USER, 
            VIEW_EDIT_USER);
    }

    /**
        * @brief Metodo magico que libera la memoria tras es uso de la clase
     */
    function __destruct(){ unset($this); }
}
$UsuarioController = new UsuarioController();
$UsuarioController->handler();
?>
