<?php
/**
* @file db_abstract_model.php
* @brief Fichero que contiene la clase abstracta DBAbstractModel con los metodos y funciones que heredaran sus subclases
* @author Eugenia Bahit
* @author Raul Guerrero
* @version 1.0
* @copyright CC 4.0 BY-NC-SA
* @date 2015-03-22
 */
abstract class DBAbstractModel {

    private static $db_host = '127.0.0.1';
    private static $db_user = 'admin';
    private static $db_pass = '';
    protected $db_name = '';
    protected $query;
    protected $rows = array();
    private $conn;
    public $mensaje = 'Hecho';
    protected $error_conn=false;
    public $error_query;

    /**
        * @brief Metodos abstractos que heredaran las subclases
     */
    abstract protected function get();
    abstract protected function set();
    abstract protected function edit();
    abstract protected function delete();

    /**
        * @brief Metodo que realiza una conexion a la base de datos y comprueba si se ha realizado correctamente
     */
    private function open_connection() {
        $this->conn = new mysqli(self::$db_host, self::$db_user, 
            self::$db_pass, $this->db_name);

        if($this->conn->connect_errno){
            $this->error_conn = true;
        }
    }

    /**
        * @brief Metodo que realiza la desconexion a la BDD
     */
    private function close_connection() { $this->conn->close(); }

    /**
        * @brief Metodo que ejecuta una query y guarda en la propiedad correspondiente el resultado
     */
    protected function execute_single_query() {
        if($_POST) {
            $this->open_connection();
            if(!$this->error_conn){
                $this->error_query = !$this->conn->query($this->query);
                if($this->error_query){
                    $this->mensaje = 'Error de query';
                }
                $this->close_connection();
            }else {
                    $this->error_query = true;
                    $this->mensaje = 'Error de conexion con la BDD';
                }
            } else {
                $this->error_query = true;
                $this->mensaje = 'Metodo no permitido';
            }
        }

        # Traer resultados de una consulta en un Array
    /**
        * @brief Metodo que recibe mas de un resultado y lo almacena en un array asociativo
     */
        protected function get_results_from_query() {
            $this->open_connection();
            if(!$this->error_conn){
                $result = $this->conn->query($this->query);
                while ($this->rows[] = $result->fetch_assoc());
                $result->close();
                $this->close_connection();
                array_pop($this->rows);
            }else{
                $this->error_query = true;
                $this->mensaje = 'Metodo no permitido'; // Modificar mensaje
            }
        }
    }
?>
